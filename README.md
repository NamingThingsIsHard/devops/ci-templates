# ci-templates

A bunch of Gitlab-CI templates for use in other projects to help set them up quickly

This repo advantage of Gitlab CI's [`include`][include] to allow reuse of jobs and pipelines.
We attempt to make the jobs as reusable and composable as possible.

## Getting started

The templates are separated per language in folders.
Further sub-folders allow separation by tooling.

In order to use the templates, you will probably need to [`include`][include] the main `base.yml`
 and that of the language+tooling e.g

:information_source: Using latest tag for `ref` is recommended,  e.g. `v0.8.0`:
[![Latest Release](https://gitlab.com/NamingThingsIsHard/devops/ci-templates/-/badges/release.svg)](https://gitlab.com/NamingThingsIsHard/devops/ci-templates/-/releases)

```yaml
include:
  - project: NamingThingsIsHard/devops/ci-templates
    ref: v0.8.0  # It's best to lock down the version in case things change
    file: base.yml
  - project: NamingThingsIsHard/devops/ci-templates
    ref: v0.8.0
    file: python/pip/base.yml
```

To add further jobs and configuration, simply add more items to the `include` list.

**Version lock**

It's recommended to select a stable git reference (tag, commit), that way you can be sure
 that your pipeline will work and things won't change.

Pointing to a branch or a commit on a non-stable branch (aka not `master`) is **not** recommended.
Branches change without warning and the commmits thereon too (
 rebasing, or the branch straight up disappearing
).

### Custom image based builds

Some projects have complicated dependencies that cannot be cached (probably system packages)
 and a lot of time is spent just installing these dependencies for jobs to run.
One way around this is to build a docker image with all dependencies and use that image in further jobs.

Include at least `base-docker.yml` that will take care of building the image 
 and defining the build image as the standard image of the pipeline.
```yaml
include:
  - project: NamingThingsIsHard/devops/ci-templates
    ref: v0.8.0  # It's best to lock down the version in case things change
    file: base-docker.yml
```

You will need a `Dockerfile` with the `base`, `ci` and `prod` targets.

Built into `base-docker.yml` is publishing a docker image to the Gitlab docker registry.

## Examples

Examples are in [examples/](examples/), separated by language.
Subfolders in the language folder are then made per combination of tools. 

[include]: https://docs.gitlab.com/ee/ci/yaml/index.html#include
