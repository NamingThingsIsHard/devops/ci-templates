# Python templates for poetry projects

# `base.yml`

This base will install and cache a virtualenv for all jobs.
Unless stated otherwise, it **will** be required to be `include`d before all other `.yml`s in this folder
 and those in the subfolders.

## Requirements

**A python image**

You will have to select a docker image from dockerhub to use this with.
Presumably a [`python`](https://hub.docker.com/_/python) image.

**pyproject.toml**

This file will contain the dependencies developers install in their local environment
 and those that the CI will use to run the same commands devs do too.
