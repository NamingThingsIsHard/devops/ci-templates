"""
The executable part of `my_package`

Use python3 -m mypackage to run
"""


import my_package

print(f"Hello my_package v{my_package.__version__}")
