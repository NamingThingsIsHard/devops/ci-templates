"""
Simple tests
"""

import unittest

import my_package


class TestMyPackage(unittest.TestCase):
    """
    Simple tests for my_package
    """

    def test_something(self):
        """Ensure the version is OK"""
        self.assertEqual(my_package.__version__, "0.1.0")


if __name__ == "__main__":
    unittest.main()
